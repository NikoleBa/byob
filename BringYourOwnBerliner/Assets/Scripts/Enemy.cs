using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float runSpeed = 1;
    private Vector3 direction;


    public bool GetRandomBoolean()
    {
        return Random.Range(-1, 1) >= 0;
    }

    void Start()
    {
        if (GetRandomBoolean())
        {
            Invoke("MoveLeft", 0);
        }
        else
        {
            Invoke("MoveRight", 0);
        }
  
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = this.transform.position + (direction * Time.deltaTime);
    }

    void MoveLeft()
    {
        direction = new Vector3(-1, 0, 0) * runSpeed;
        Invoke("MoveRight", 1);
    }


    void MoveRight()
    {
        direction = new Vector3(1, 0, 0) * runSpeed;
        Invoke("MoveLeft", 1);

    }
}

