using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int health = 3;
    public GameObject[] berliner;
    private bool isGameover = false;
    public GameObject gameOver;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && isGameover)
        {
            GetComponent<Player>().Reset();
            health = 3;
            UpdateHealth();
            isGameover = false;
        }
    }

    public void PlayerHit()
    {
        health--;
        UpdateHealth();
    }

    void UpdateHealth()
    {
        if (health == 3)
        {
            berliner[0].SetActive(true);
            berliner[1].SetActive(true);
            berliner[2].SetActive(true);
        }

        else if (health==2)
        {
            berliner[0].SetActive(true);
            berliner[1].SetActive(true);
            berliner[2].SetActive(false);
        }

        else if (health == 1)
        {
            berliner[0].SetActive(true);
            berliner[1].SetActive(false);
            berliner[2].SetActive(false);
        }

        else if (health == 0)
        {
            berliner[0].SetActive(false);
            berliner[1].SetActive(false);
            berliner[2].SetActive(false);

            GameOver();
        }
    }

    void GameOver()
    {
        gameOver.SetActive(true);
        Time.timeScale = 0;
        GetComponent<Player>().motor.Stop();
        isGameover = true;
    }
}
