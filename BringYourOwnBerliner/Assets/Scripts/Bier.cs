using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bier : MonoBehaviour
{
    public AudioSource plopp;
    public Player player;
    //public BierUI ui;
    // Start is called before the first frame update
    void Awake()
    {
        this.player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            plopp.Play();
            player.BeerCollected();
            this.gameObject.SetActive(false);

        }
    }
}
