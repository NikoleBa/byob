using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BierUi : MonoBehaviour
{
    private Player player;


    public GameObject[] bottles;
    // Start is called before the first frame update
    void Awake()
    {
        this.player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void BottleCollected()
    {
        if (player.beercount == 6)
        {
            bottles[0].SetActive(true);
            bottles[1].SetActive(true);
            bottles[2].SetActive(true);
            bottles[3].SetActive(true);
            bottles[4].SetActive(true);
            bottles[5].SetActive(true);
        }

        else if (player.beercount == 5)
        {
            bottles[0].SetActive(true);
            bottles[1].SetActive(true);
            bottles[2].SetActive(true);
            bottles[3].SetActive(true);
            bottles[4].SetActive(true);
            bottles[5].SetActive(false);
        }

        else if (player.beercount == 4)
        {
            bottles[0].SetActive(true);
            bottles[1].SetActive(true);
            bottles[2].SetActive(true);
            bottles[3].SetActive(true);
            bottles[4].SetActive(false);
            bottles[5].SetActive(false);
        }

        else if (player.beercount == 3)
        {
            bottles[0].SetActive(true);
            bottles[1].SetActive(true);
            bottles[2].SetActive(true);
            bottles[3].SetActive(false);
            bottles[4].SetActive(false);
            bottles[5].SetActive(false);
        }

        else if (player.beercount == 2)
        {
            bottles[0].SetActive(true);
            bottles[1].SetActive(true);
            bottles[2].SetActive(false);
            bottles[3].SetActive(false);
            bottles[4].SetActive(false);
            bottles[5].SetActive(false);
        }

        else if (player.beercount == 1)
        {
            bottles[0].SetActive(true);
            bottles[1].SetActive(false);
            bottles[2].SetActive(false);
            bottles[3].SetActive(false);
            bottles[4].SetActive(false);
            bottles[5].SetActive(false);

        }

        else if (player.beercount == 0)
        {
            bottles[0].SetActive(false);
            bottles[1].SetActive(false);
            bottles[2].SetActive(false);
            bottles[3].SetActive(false);
            bottles[4].SetActive(false);
            bottles[5].SetActive(false);

        }
    }
}
