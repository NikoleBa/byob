using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasEnable : MonoBehaviour
{
    public GameObject collectCanvas;


    private void OnTriggerEnter2D(Collider2D other)
    {
        //player enter the item collider > give the player the ticket by setting the bool to "true", then play the sound and destroy it!
        if (other.gameObject.CompareTag("Player"))
        {
            collectCanvas.SetActive(true);

        }
    }
}
