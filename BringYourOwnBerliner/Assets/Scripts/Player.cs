using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    Vector3 originalPos;
    public float runSpeed;
    public float m_JumpForce = 400f;
    public bool jump = false;
    public bool canMove = true;
    public AudioSource jumpSound;
    [SerializeField]
    private Transform m_GroundCheck;
    [SerializeField]
    private LayerMask m_WhatIsGround;
    const float k_GroundedRadius = .2f;
    private bool m_Grounded;
    private Rigidbody2D m_Rigidbody2D;
    public int beercount = 0;
    public BierUi bierUi;
    private bool allBeer = false;
    public GameObject[] alle;
    public GameObject gameOver;
    public AudioSource motor;
    public AudioSource ouch;

    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

    }

    void Start()
    {
        originalPos = transform.position;

    }


    // Update is called once per frame
    void Update()
    {
        this.transform.position = this.transform.position + (new Vector3(runSpeed, 0, 0) * Time.deltaTime);

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
    }

    void FixedUpdate()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
            }
        }

        if (!canMove)
        {
            return;
        }
        // Move our character
        Jump(runSpeed * Time.fixedDeltaTime, jump);
        jump = false;
    }


    public void Jump(float runSpeed, bool jump)
    {
        // If the player should jump...
        if (m_Grounded && jump)
        {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
            jumpSound.Play();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Enemy"))
        {
            GetComponent<Health>().PlayerHit();
            ouch.Play();
        }
    }

    public void Reset()
    {
        this.transform.position = originalPos;
        Time.timeScale = 1;
        gameOver.SetActive(false);
        beercount = 0;
        bierUi.BottleCollected();
        for (int i = 0; i < alle.Length; i++)
        {
            alle[i].SetActive(true);
        }
        motor.Play();

    }

    public void BeerCollected()
    {
        beercount++;
        bierUi.BottleCollected();

        if (beercount == 6)
        {
            allBeer = true;

        }

        else
        {
            allBeer = false;
        }
    }

    public void CheckEnding()
    {
        if (allBeer)
        {
            SceneManager.LoadScene("GoodEnd");
        }

        else
        {
            SceneManager.LoadScene("BadEnd");
        }

    }
}

